module.exports = {
  production: {
    certUrl: __dirname + "/ssl/production/",
    keyUrl: __dirname + "/ssl/production/",
    dbUrl: "",
    portHTTP: 80,
    portHTTPS: 443,
  },
  development: {
    certUrl: __dirname + "/ssl/development/localhost.crt",
    keyUrl: __dirname + "/ssl/development/localhost.key",
    dbUrl:
      "mongodb+srv://admin:250789@cluster0-2btph.gcp.mongodb.net/dyma-chat?retryWrites=true&w=majority",
    portHTTP: 3000,
    portHTTPS: 3001,
  },
};
